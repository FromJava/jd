package ru.java.rush.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import ru.java.rush.entities.FruitEntity;
import ru.java.rush.repositories.FruitRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FruitService {

    private final FruitRepository fruitRepository;

    public Optional<FruitEntity> getById(Integer id){
       return fruitRepository.findById(id);
    }

    public void delById(Integer id){
        fruitRepository.deleteById(id);
    }

    public Boolean exist(Example<? extends FruitEntity> example){
        return fruitRepository.exists(example);
    }

    public void save(FruitEntity fruitEntity){
        fruitRepository.save(fruitEntity);
    }

    public List<FruitEntity> getAll(){
       return fruitRepository.findAll();
    }

    public void saveAll(List<FruitEntity> fruits){
        fruitRepository.saveAll(fruits);
    }

    public List<String> joinString(){
       return fruitRepository.joinSting();
    }

    public List<FruitEntity> joinFruit(){
        return fruitRepository.joinFruit();
    }

    public List<String> joinProvider(){
        return fruitRepository.joinProvider();
    }
}
